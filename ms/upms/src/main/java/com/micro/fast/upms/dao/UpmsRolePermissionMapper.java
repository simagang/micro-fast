package com.micro.fast.upms.dao;

import com.micro.fast.common.dao.SsmMapper;
import com.micro.fast.upms.pojo.UpmsRolePermission;

import java.util.List;

public interface UpmsRolePermissionMapper extends SsmMapper<UpmsRolePermission,Integer> {
    @Override
    int deleteByPrimaryKey(Integer rolePermissionId);

    @Override
    int insert(UpmsRolePermission record);

    @Override
    int insertSelective(UpmsRolePermission record);

    @Override
    UpmsRolePermission selectByPrimaryKey(Integer rolePermissionId);

    @Override
    int updateByPrimaryKeySelective(UpmsRolePermission record);

    @Override
    int updateByPrimaryKey(UpmsRolePermission record);

}